<?php

/**
 * @file
 * Contains layout_builder_quick__add.module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\file\Entity\File;
use Drupal\layout_builder_quick_add\LayoutBuilderQuickAddHelper;

/**
 * Implements hook_help().
 */
function layout_builder_quick_add_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
      // Main module help for the layout_builder_quick__add module.
    case 'help.page.layout_builder_quick__add':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Layout Builder Quick Add') . '</p>';
      return $output;

    default:
  }
}

/**
 * Implementation of hook_element_info_alter().
 */
function layout_builder_quick_add_element_info_alter(array &$types) {
  $types['layout_builder']['#pre_render'][] = [LayoutBuilderQuickAddHelper::class, 'layoutBuilderPreRender'];
}

/**
 * Implements hook_theme().
 */
function layout_builder_quick_add_theme() {
  return [
    'quick_add_blocks_listing' => [
      'variables' => [
        'content' => NULL,
        'see_more' => NULL,
        'cancel' => NULL,
      ],
      'render element' => 'children',
    ],
    'quick_add_blocks_listing_item' => [
      'variables' => [
        'plugin_id' => NULL,
        'label' => NULL,
        'description' => NULL,
        'screenshot' => NULL,
        'multiple_view_mode_message' => NULL,
        'icon' => NULL,
        'link' => NULL,
      ],
      'render element' => 'children',
    ],
  ];
}

/**
 * Implements hook_form_alter().
 */
function layout_builder_quick_add_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if ($form_id == 'block_content_type_edit_form') {
    foreach (array_keys($form['actions']) as $action) {
      if ($action != 'preview' && isset($form['actions'][$action]['#type']) && $form['actions'][$action]['#type'] === 'submit') {

        $entityType = $form_state->getFormObject()->getEntity();
        $icon = $entityType->getThirdPartySetting('layout_builder_quick_add', 'icon', NULL);
        $form['layout_builder_quick_add_icon'] = [
          '#type' => 'managed_file',
          '#title' => t('Block type icon'),
          '#upload_location' => 'public://layout_builder_quick_add',
          '#upload_validators' => [
            'file_validate_extensions' => ['jpg, png, jpeg, svg'],
          ],
          '#default_value' => !empty($icon) ? [$icon] : [],
          '#description' => t('This icon will be used '),
        ];

        $form['actions'][$action]['#submit'][] = 'layout_builder_quick_add_form_submit';
      }
    }
  }
}

/**
 * Submit callback to save the icon and set the third part setting.
 */
function layout_builder_quick_add_form_submit(array $form, FormStateInterface $form_state) {

  $form_file = $form_state->getValue('layout_builder_quick_add_icon', 0);
  if (isset($form_file[0]) && !empty($form_file[0])) {
    $file = File::load($form_file[0]);
    $file->setPermanent();
    $file->save();

    $entityType = $form_state->getFormObject()->getEntity();
    $entityType->setThirdPartySetting('layout_builder_quick_add', 'icon', $file->id());
    $entityType->save();
  }
}

/**
 * Implements hook_preprocess_HOOK() for page_attachments.
 */
function layout_builder_quick_add_page_attachments_alter(&$page) {
  // Add potential needed libraries, for css variables for example.
  $config = \Drupal::config('layout_builder_quick_add.settings');
  $theme = $config->get('theme');
  switch ($theme) {
    case 'gin':
      // Set the proper gin required settings for it to work.
      // For better support, use gin_toolbar module that is doing a similar
      // job.
      if (_layout_builder_quick_add_theme_is_active('gin') && _layout_builder_quick_add_check_path()) {
        $settings = \Drupal::classResolver(\Drupal\gin\GinSettings::class);

        $page['#attached']['library'][] = 'gin/gin_base';
        $page['#attached']['library'][] = 'gin/gin_accent';
        $page['#attached']['library'][] = 'gin/gin_init';
        $page['#attached']['drupalSettings']['gin']['darkmode'] = $settings->get('enable_darkmode');
        $page['#attached']['drupalSettings']['gin']['darkmode_class'] = 'gin--dark-mode';
        $page['#attached']['drupalSettings']['gin']['preset_accent_color'] = $settings->get('preset_accent_color');
        $page['#attached']['drupalSettings']['gin']['accent_color'] = $settings->get('accent_color');
        $page['#attached']['drupalSettings']['gin']['preset_focus_color'] = $settings->get('preset_focus_color');
        $page['#attached']['drupalSettings']['gin']['focus_color'] = $settings->get('focus_color');
        $page['#attached']['drupalSettings']['gin']['highcontrastmode'] = $settings->get('high_contrast_mode');
        $page['#attached']['drupalSettings']['gin']['highcontrastmode_class'] = 'gin--high-contrast-mode';
        $page['#attached']['drupalSettings']['gin']['toolbar_variant'] = $settings->get('classic_toolbar');
      }
      break;
    case 'claro':
      if (_layout_builder_quick_add_theme_is_active('claro') && _layout_builder_quick_add_check_path()) {
        //$page['#attached']['library'][] = 'claro/global-styling';
      }
      break;
  }
}

/**
 * Helper function to check if we're on the right paths.
 */
function _layout_builder_quick_add_check_path() {
  // Get path from Route.
  $route = \Drupal::routeMatch()->getRouteName();

  $lb_routes_prefix = [
    'layout_builder.overrides.',
    'layout_builder.defaults.',
  ];

  foreach ($lb_routes_prefix as $lb_prefix) {
    if (substr($route, 0, strlen($lb_prefix)) === $lb_prefix) {
      return TRUE;
    }
  }
  return FALSE;
}

/**
 * Helper function for check if Theme is active.
 */
function _layout_builder_quick_add_theme_is_active($theme) {
  $theme_handler = \Drupal::service('theme_handler')->listInfo();

  // Check if set as frontend theme.
  $frontend_theme_name = \Drupal::config('system.theme')->get('default');

  // Check if base themes are set.
  if (isset($theme_handler[$frontend_theme_name]->base_themes)) {
    $frontend_base_themes = $theme_handler[$frontend_theme_name]->base_themes;
  }

  // Add theme name to base theme array.
  $frontend_base_themes[$frontend_theme_name] = $frontend_theme_name;

  // Check if set as admin theme.
  $admin_theme_name = \Drupal::config('system.theme')->get('admin');

  // Admin theme will have no value if is set to use the default theme.
  if ($admin_theme_name) {
    $admin_base_themes = isset($theme_handler[$admin_theme_name]->base_themes) ? $theme_handler[$admin_theme_name]->base_themes : [];
    $admin_base_themes[$admin_theme_name] = $admin_theme_name;
  }
  else {
    $admin_base_themes = $frontend_base_themes;
  }

  $theme_activated = array_key_exists($theme, $admin_base_themes);

  return $theme_activated;
}

