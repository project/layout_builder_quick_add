<?php

namespace Drupal\layout_builder_quick_add\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class QuickAddConfigForm.
 */
class QuickAddConfigForm extends ConfigFormBase {

  /**
   * Drupal\Core\Block\BlockManagerInterface definition.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $pluginManagerBlock;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->pluginManagerBlock = $container->get('plugin.manager.block');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'layout_builder_quick_add.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'quick_add_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('layout_builder_quick_add.settings');

    $form['lbqa_settings'] = [
      '#type' => 'vertical_tabs',
      '#title' => $this->t('Layout Builder Quick Add Settings'),
    ];

    $form['general_tab'] = [
      '#type' => 'details',
      '#title' => 'General',
      '#group' => 'lbqa_settings',
      '#tree' => true,
    ];

    $form['general_tab']['theme'] = [
      '#type' => 'select',
      '#title' => $this->t('Theme'),
      '#description' => $this->t('Pick the theme you want to apply or none if you want to style it yourself.'),
      '#default_value' => $config->get('theme'),
      '#options' => [
        'default' => $this->t('Default'),
        'none' => $this->t('None'),
        'claro' => $this->t('Claro'),
        'gin' => $this->t('Gin'),
      ],
    ];

    $form['general_tab']['display_description'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Display block description'),
      '#description' => $this->t('Check this box to display the description of the block on the quick add links.'),
      '#default_value' => $config->get('display_description'),
    ];

    $form['general_tab']['multiple_view_mode'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show multiple view mode message'),
      '#description' => $this->t('Check this box to show a message when the block has multiple view modes.'),
      '#default_value' => $config->get('multiple_view_mode'),
    ];

    $form['general_tab']['multiple_view_mode_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Multiple wiew mode message'),
      '#description' => $this->t('The message that will be shown when a block has multiple view modes.'),
      '#default_value' => $config->get('multiple_view_mode_message'),
      '#states' => [
        'invisible' => [
          ':input[name="general_tab[multiple_view_mode]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    $form['enabled_blocks_tab'] = [
      '#type' => 'details',
      '#title' => 'Enabled blocks',
      '#group' => 'lbqa_settings',
      '#tree' => true,
    ];

    $options = [];
    if ($this->entityTypeManager->hasDefinition('block_content_type') && $types = $this->entityTypeManager->getStorage('block_content_type')->loadMultiple()) {
      if (count($types) > 0) {
        foreach ($types as $type) {
          $plugin_id = $type->id();
          $options[$plugin_id] = $type->label();
        }
      }
    }

    $form['enabled_blocks_tab']['inline_blocks'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Inline Blocks'),
      '#description' => $this->t('Select the blocks that you can add quickly in layout builder.'),
      '#options' => $options,
      '#default_value' => $config->get('inline_blocks'),
    ];

    $form['order'] = [
      '#type' => 'details',
      '#title' => 'Order',
      '#group' => 'lbqa_settings',
      '#tree' => true,
    ];

    $form['order']['help'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Configure the order of the blocks that you want to display.'),
    ];

    $form['order']['table-row'] = [
      '#type' => 'table',
      '#header' => [
        $this
          ->t('Name'),
        $this
          ->t('Weight'),
      ],
      '#empty' => $this
        ->t('There are no blocks enabled.'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'table-sort-weight',
        ],
      ],
    ];

    $enabled_blocks = $config->get('inline_blocks');
    $order = $config->get('blocks_order');

    if (is_array($order) && count($order) > 0) {
      foreach (array_keys($order) as $plugin_id) {
        if (!empty($enabled_blocks[$plugin_id])) {
          $form['order']['table-row'][$plugin_id] = $this->createSortableWeight($order, $options, $plugin_id);
        }
      }
    }

    // If the blocks are not in the order, add them at the end.
    // It happens when creating new blocks for example.
    if (is_array($enabled_blocks) && count($enabled_blocks) > 0) {
      foreach (array_keys($enabled_blocks) as $plugin_id) {
        // Check that it has not been set yet.
        if (empty($form['order']['table-row'][$plugin_id]) && !empty($enabled_blocks[$plugin_id])) {
          $form['order']['table-row'][$plugin_id] = $this->createSortableWeight($order, $enabled_blocks, $plugin_id);
        }
      }
    }

    $form['screenshots'] = [
      '#type' => 'details',
      '#title' => 'Screenshots',
      '#group' => 'lbqa_settings',
      '#tree' => TRUE,
    ];

    $form['screenshots']['help'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Your can upload a screenshot for each block. They will appear in the tooltip in order to help users to have a quick preview of the block.'),
    ];

    if ($this->entityTypeManager->hasDefinition('block_content_type') && $types = $this->entityTypeManager->getStorage('block_content_type')->loadMultiple()) {
      if (count($types) > 0) {
        foreach ($types as $type) {
          $config_name = 'screenshot:' . $type->id();
          $form['screenshots'][$config_name] = array(
            '#type' => 'managed_file',
            '#upload_location' => 'public://lbqa/screenshots',
            '#title' => $type->label(),
            '#upload_validators' => [
              'FileExtension' => ['extensions' => 'gif png jpg jpeg'],
            ],
            '#default_value' => !empty($config->get($config_name)) ? $config->get($config_name) : '',
            '#required' => FALSE,
          );
        }
      }
    }

    $form['preview'] = [
      '#type' => 'details',
      '#title' => 'Preview',
      '#group' => 'lbqa_settings',
      '#tree' => TRUE,
    ];

    $form['preview']['content'] = \Drupal::service('layout_builder_quick_add.helper')->getPreview();

    return parent::buildForm($form, $form_state);
  }

  private function createSortableWeight($order, $enabled_blocks, $plugin_id) {
    // TableDrag: Mark the table row as draggable.
    $element['#attributes']['class'][] = 'draggable';

    // TableDrag: Sort the table row according to its existing/configured
    // weight.
    $weight = isset($order[$plugin_id]['weight']) ? intval($order[$plugin_id]['weight']) : 0;
    $element['#weight'] = $weight;

    // Some table columns containing raw markup.
    $element['name'] = [
      '#markup' => $enabled_blocks[$plugin_id] . ' (' . $plugin_id . ')',
    ];

    // TableDrag: Weight column element.
    $element['weight'] = [
      '#type' => 'weight',
      '#title' => $this
        ->t('Weight for @title', [
          '@title' => $enabled_blocks[$plugin_id],
        ]),
      '#title_display' => 'invisible',
      '#default_value' => $weight,
      // Classify the weight element for #tabledrag.
      '#attributes' => [
        'class' => [
          'table-sort-weight',
        ],
      ],
    ];
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();

    // When enabling new blocks, the weight is not set. Set weight 0 for them.
    foreach ($values['enabled_blocks_tab']['inline_blocks'] as $plugin_id => $block) {
      if (!empty($values['enabled_blocks_tab']['inline_blocks'][$plugin_id])) {
        if (!is_array($values['order']['table-row'])) {
          $values['order']['table-row'] = [];
        }
        $values['order']['table-row'][$plugin_id] = [
          '#weight' => 0,
        ];
      }
    }

    $config = $this->config('layout_builder_quick_add.settings');
    $config->set('inline_blocks', $values['enabled_blocks_tab']['inline_blocks']);
    $config->set('blocks_order', $values['order']['table-row']);
    $config->set('theme', $values['general_tab']['theme']);
    $config->set('display_description', $values['general_tab']['display_description']);
    $config->set('multiple_view_mode', $values['general_tab']['multiple_view_mode']);
    $config->set('multiple_view_mode_message', $values['general_tab']['multiple_view_mode_message']);

    foreach ($values['screenshots'] as $screenshot_key => $screenshot) {
      if (!empty($screenshot[0])) {
        $file = File::load($screenshot[0]);
        $file->setPermanent();
        $file->save();
      }
      $config->set($screenshot_key, $values['screenshots'][$screenshot_key]);
    }

    $config->save();
  }

}

function cmp($a, $b) {
  if ($a == $b) {
    return 0;
  }
  return ($a < $b) ? -1 : 1;
}


