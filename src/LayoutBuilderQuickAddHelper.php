<?php

namespace Drupal\layout_builder_quick_add;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

/**
 * Class LayoutBuilderQuickAddHelper.
 */
class LayoutBuilderQuickAddHelper implements TrustedCallbackInterface {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Block\BlockManagerInterface definition.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $pluginManagerBlock;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Entity\EntityDisplayRepositoryInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected $entityDisplayRepository;

  /**
   * Drupal\Core\Config\Config definition.
   *
   * @var \Drupal\Core\Config\Config
   */
  public Config $layoutBuilderQuickAddConfig;

  /**
   * Constructs a new LayoutBuilderQuickAddHelper object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, BlockManagerInterface $plugin_manager_block, EntityTypeManagerInterface $entity_type_manager, EntityDisplayRepositoryInterface $entity_display_repository) {
    $this->configFactory = $config_factory;
    $this->pluginManagerBlock = $plugin_manager_block;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->layoutBuilderQuickAddConfig = $config_factory->get('layout_builder_quick_add.settings');
  }

  public function getPreview() {

    $enabled_blocks = $this->getQuickAddEnabledBlocks();
    $items = [];
    foreach ($enabled_blocks as $enabled_block) {
      $items[] = [
        '#theme' => 'quick_add_blocks_listing_item',
        '#plugin_id' => $enabled_block['plugin_id'],
        '#label' => $enabled_block['label'],
        '#description' => $enabled_block['description'],
        '#multiple_view_mode_message' => $this->getViewModeMessage($enabled_block['id']),
        '#screenshot' => $this->getScreenshot($enabled_block['id'], $enabled_block['label']),
        '#icon' => $enabled_block['icon'],
        '#link' => [
          '#theme' => 'links',
          '#links' => [
            [
              'title' => $enabled_block['label'],
              // Just use a fake link since we just want to preview.
              'url' => Url::fromUri('https://www.drupal.org/project/layout_builder_quick_add'),
            ]
          ],
        ],
      ];
    }

    $content = [
      '#theme' => 'quick_add_blocks_listing',
      '#content' => $items,
    ];

    $theme = $this->layoutBuilderQuickAddConfig->get('theme');
    if ($theme !== 'none') {
      $content['#attached']['library'][] = 'layout_builder_quick_add/' . $theme;
    }

    return $content;
  }

  public function getScreenshot($id, $label) {
    $screenshot = [];
    $config_id = 'screenshot:' . $id;
    $screenshot_config = $this->layoutBuilderQuickAddConfig->get($config_id);
    if (!empty($screenshot_config)) {
      $file = File::load($screenshot_config[0]);
      if (!empty($file)) {
        $screenshot = [
          '#theme' => 'image',
          '#uri' => $file->createFileUrl(FALSE),
          '#alt' => $this->t('Screenshot of @block', ['@block' => $label]),
          '#title' => $this->t('Screenshot of @block', ['@block' => $label]),
        ];
      }
    }
    return $screenshot;
  }

  public function getViewModeMessage($id) {
    $message = '';
    if ($this->layoutBuilderQuickAddConfig->get('multiple_view_mode')) {
      $view_modes = $this->entityDisplayRepository->getViewModeOptionsByBundle('block_content', $id);

      if (count($view_modes) > 1) {
        $message = $this->layoutBuilderQuickAddConfig->get('multiple_view_mode_message');
      }
    }

    return $message;
  }

  public function getQuickAddEnabledBlocks() {
    $display_description = $this->layoutBuilderQuickAddConfig->get('display_description');
    $order = $this->layoutBuilderQuickAddConfig->get('blocks_order');

    $enabled_blocks = [];
    if ($this->entityTypeManager->hasDefinition('block_content_type') && $types = $this->entityTypeManager->getStorage('block_content_type')->loadMultiple()) {
      if (is_array($order)) {
        // If we have a configured order.
        if (!empty($order)) {
          foreach (array_keys($order) as $block) {
            if (!empty($types[$block])) {
              $enabled_blocks[] = $this->getBlockTypeData($types, $block, $display_description);
            }
          }
        }
        // If the order is empty, typically happens when enabling the module,
        // show all the block types.
        else {
          foreach ($types as $type_key => $type) {
            $enabled_blocks[] = $this->getBlockTypeData($types, $type_key, $display_description);
          }
        }
      }
    }
    return $enabled_blocks;
  }

  public function getBlockTypeData($types, $id, $display_description) {
    $type = $types[$id];
    $icon_id = $type->getThirdPartySetting('layout_builder_quick_add', 'icon', NULL);

    $icon = (!empty($icon_id)) ? File::load($icon_id)->url() : NULL;
    return [
      'id' => $type->id(),
      'plugin_id' => 'inline_block:' . $type->id(),
      'label' => $type->label(),
      'description' => (method_exists($type, 'getDescription') && $display_description) ? $type->getDescription() : '',
      'icon' => $icon,
    ];
  }

  public static function layoutBuilderPreRender(array $element) {
    $lb = $element['layout_builder'];

    foreach (Element::children($lb) as $section) {
      if (isset($lb[$section]['layout-builder__section'])) {
        foreach (Element::children($lb[$section]['layout-builder__section']) as $region) {
          // Alter the add block link.
          if (!empty($lb[$section]['layout-builder__section'][$region]['layout_builder_add_block'])) {
            $original_url = $element['layout_builder'][$section]['layout-builder__section'][$region]['layout_builder_add_block']['link']['#url'];
            $attributes = $original_url->getOption('attributes');
            $kept_attributes = [
              'attributes' => [
                'class' => $attributes['class'],
              ],
            ];
            $route_parameters = $original_url->getRouteParameters();
            $element['layout_builder'][$section]['layout-builder__section'][$region]['layout_builder_add_block']['link']['#url'] = Url::fromRoute('layout_builder_quick_add.add_blocks', $route_parameters, $kept_attributes);
          }
        }
      }
    }

    $theme = \Drupal::config('layout_builder_quick_add.settings')->get('theme');
    if ($theme !== 'none') {
      $element['layout_builder']['#attached']['library'][] = 'layout_builder_quick_add/' . $theme;
    }

  return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['layoutBuilderPreRender'];
  }

}
