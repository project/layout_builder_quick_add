<?php

namespace Drupal\layout_builder_quick_add\Controller;

use Drupal\Core\Ajax\AfterCommand;
use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\RemoveCommand;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\layout_builder\LayoutBuilderHighlightTrait;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\layout_builder_quick_add\LayoutBuilderQuickAddHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LayoutBuilderQuickAddController extends ControllerBase {

  use LayoutBuilderHighlightTrait;
  use AjaxHelperTrait;

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\layout_builder_quick_add\LayoutBuilderQuickAddHelper definition.
   *
   * @var \Drupal\layout_builder_quick_add\LayoutBuilderQuickAddHelper
   */
  protected $layoutBuilderQuickAddHelper;

  /**
   * Constructs a \Drupal\layout_builder_quick_add\LayoutBuilderQuickAddController object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LayoutBuilderQuickAddHelper $layout_builder_quick_add_helper) {
    $this->entityTypeManager = $entity_type_manager;
    $this->layoutBuilderQuickAddHelper = $layout_builder_quick_add_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('layout_builder_quick_add.helper')
    );
  }

  /**
   * Insert the quick add blocks links.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The delta of the section to splice.
   * @param string $region
   *   The region the block is going in.
   *
   * @return array
   *   A render array.
   */
  public function addBlocks(SectionStorageInterface $section_storage, $delta, $region) {

    $id = $this->blockAddHighlightId($delta, $region);
    $selector = '[data-layout-builder-highlight-id="' . $id . '"] .layout-builder__link--add';

    $content = [
      '#theme' => 'quick_add_blocks_listing',
      '#content' => $this->getQuickAddBlocksListingItems($section_storage, $delta, $region),
      '#see_more' => $this->getSeeMoreLink($section_storage, $delta, $region),
      '#cancel' => $this->getCancelLink($section_storage, $delta, $region),
    ];
    $rendered_content = $content;

    $response = new AjaxResponse();
    $response->addCommand(new AfterCommand($selector, $rendered_content));
    $response->addCommand(new InvokeCommand($selector, 'attr', ['style', 'display: none !important;']));
    return $response;
  }

  public function cancelAddBlocks(SectionStorageInterface $section_storage, $delta, $region) {
    $id = $this->blockAddHighlightId($delta, $region);
    $selector_add_link = '[data-layout-builder-highlight-id="' . $id . '"] .layout-builder__link--add';
    $selector = '[data-layout-builder-highlight-id="' . $id . '"] .js-layout-builder__quick-add-blocks';
    $response = new AjaxResponse();
    $response->addCommand(new RemoveCommand($selector));
    $response->addCommand(new CssCommand($selector_add_link, ['display' => 'inline-block']));
    return $response;
  }

  public function getQuickAddBlocksListingItems($section_storage, $delta, $region) {
    $attributes = $this->getAjaxAttributes();
    $attributes['class'][] = 'js-layout-builder-block-link';

    $enabled_blocks = $this->layoutBuilderQuickAddHelper->getQuickAddEnabledBlocks();
    $items = [];
    foreach ($enabled_blocks as $enabled_block) {
      $items[] = [
        '#theme' => 'quick_add_blocks_listing_item',
        '#plugin_id' => $enabled_block['plugin_id'],
        '#label' => $enabled_block['label'],
        '#description' => $enabled_block['description'],
        '#multiple_view_mode_message' => $this->layoutBuilderQuickAddHelper->getViewModeMessage($enabled_block['id']),
        '#screenshot' => $this->layoutBuilderQuickAddHelper->getScreenshot($enabled_block['id'], $enabled_block['label']),
        '#icon' => $enabled_block['icon'],
        '#link' => [
          '#theme' => 'links',
          '#links' => [
            [
              'title' => $enabled_block['label'],
              'url' => Url::fromRoute(
                'layout_builder.add_block',
                [
                  'section_storage_type' => $section_storage->getStorageType(),
                  'section_storage' => $section_storage->getStorageId(),
                  'delta' => $delta,
                  'region' => $region,
                  'plugin_id' => $enabled_block['plugin_id'],
                ]
              ),
              'attributes' => $attributes,
            ]
          ],
        ],
      ];
    }

    return $items;
  }

  public function getSeeMoreLink(SectionStorageInterface $section_storage, $delta, $region) {
    $storage_type = $section_storage->getStorageType();
    $storage_id = $section_storage->getStorageId();
    $section = $section_storage->getSection($delta);
    $layout = $section->getLayout();
    $layout_settings = $section->getLayoutSettings();
    $section_label = !empty($layout_settings['label']) ? $layout_settings['label'] : $this->t('Section @section', ['@section' => $delta + 1]);
    $layout_definition = $layout->getPluginDefinition();
    $region_labels = $layout_definition->getRegionLabels();

    return [
      '#type' => 'link',
      // Add one to the current delta since it is zero-indexed.
      '#title' => $this->t('See more blocks <span class="visually-hidden">for @section, @region region</span>', ['@section' => $section_label, '@region' => $region_labels[$region]]),
      '#url' => Url::fromRoute(
        'layout_builder.choose_block',
        [
          'section_storage_type' => $storage_type,
          'section_storage' => $storage_id,
          'delta' => $delta,
          'region' => $region,
        ],
        [
          'attributes' => [
            'class' => [
              'use-ajax',
              'layout-builder__link',
            ],
            'data-dialog-type' => 'dialog',
            'data-dialog-renderer' => 'off_canvas',
          ],
        ]
      ),
    ];
  }

  public function getCancelLink(SectionStorageInterface $section_storage, $delta, $region) {
    $storage_type = $section_storage->getStorageType();
    $storage_id = $section_storage->getStorageId();
    $section = $section_storage->getSection($delta);
    $layout = $section->getLayout();
    $layout_settings = $section->getLayoutSettings();
    $section_label = !empty($layout_settings['label']) ? $layout_settings['label'] : $this->t('Section @section', ['@section' => $delta + 1]);
    $layout_definition = $layout->getPluginDefinition();
    $region_labels = $layout_definition->getRegionLabels();

    return [
      '#type' => 'link',
      // Add one to the current delta since it is zero-indexed.
      '#title' => $this->t('Cancel <span class="visually-hidden">for @section, @region region</span>', ['@section' => $section_label, '@region' => $region_labels[$region]]),
      '#url' => Url::fromRoute(
        'layout_builder_quick_add.cancel_add_blocks',
        [
          'section_storage_type' => $storage_type,
          'section_storage' => $storage_id,
          'delta' => $delta,
          'region' => $region,
        ],
        [
          'attributes' => [
            'class' => [
              'use-ajax',
              'layout-builder__link',
            ],
          ],
        ]
      ),
    ];
  }

  /**
   * Get dialog attributes if an ajax request.
   *
   * @return array
   *   The attributes array.
   */
  protected function getAjaxAttributes() {
    if ($this->isAjax()) {
      return [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'dialog',
        'data-dialog-renderer' => 'off_canvas',
      ];
    }
    return [];
  }

}
